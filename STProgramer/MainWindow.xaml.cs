﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using LibUsbDotNet;
using LibUsbDotNet.LibUsb;
using LibUsbDotNet.Main;

namespace STProgramer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            //UsbDeviceCollection usbDeviceCollection;
            using (var context = new UsbContext())
            {
                context.SetDebugLevel(LogLevel.Info);

                var usbDeviceCollection = context.List();
                foreach (var dev in usbDeviceCollection)
                {
                    Console.WriteLine(dev.ProductId);
                }
            }

            

        }
    }
}
