﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LibUsbDotNet;
using LibUsbDotNet.LibUsb;
using LibUsbDotNet.Main;

namespace STProgramer
{
    public class STLinkSharp
    {
        #region Configurations
        private static readonly UsbDeviceFinder STLinkDevice = new UsbDeviceFinder(0x0483, 0x3748);
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor: find the first device and set the device to the programming mode
        /// </summary>
        public STLinkSharp()
        {

        }

        /// <summary>
        /// De-constructor: close the device.
        /// </summary>
        ~STLinkSharp()
        {

        }
        #endregion#

        #region Private members 
        private readonly UsbEndpointReader linkreader;
        private readonly UsbEndpointWriter linkwriter;
        private void WriteLink(byte[] data)
        {
            
        }

        private byte[] ReadLink(int len)
        {
            return null;
        }
        #endregion

        #region Method

        #endregion
    }
}
